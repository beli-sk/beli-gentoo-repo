# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Store and restore metadata of files, directories, links in a tree"
HOMEPAGE="https://github.com/przemoc/metastore"
SRC_URI="https://github.com/przemoc/metastore/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="amd64 ~arm ~ppc64 ~x86"
IUSE=""
SLOT="0"

src_install() {
	emake install PREFIX="${D}/usr" DOCDIR="${D}/usr/share/doc/${P}"
}
