# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="Store and restore metadata of files, directories, links in a tree"
HOMEPAGE="https://github.com/przemoc/metastore"
EGIT_REPO_URI="https://github.com/przemoc/metastore.git"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~arm ~ppc64 ~x86"
IUSE=""
SLOT="0"

case ${PV} in
"9999")
	EGIT_BRANCH="master"
	;;
*)
	EGIT_BRANCH="master"
	;;
esac

src_install() {
	emake install PREFIX="${D}/usr" DOCDIR="${D}/usr/share/doc/${P}"
}
