# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="git remote helper to push and pull repositories encrypted with GnuPG"
HOMEPAGE="https://spwhitton.name/tech/code/git-remote-gcrypt/"
SRC_URI="https://git.spwhitton.name/git-remote-gcrypt/snapshot/${P}.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="amd64 x86"
RESTRICT="binchecks strip"

DEPEND="dev-vcs/git"
RDEPEND="${DEPEND}"

src_install() {
	dodoc README.rst
	dobin git-remote-gcrypt
}
