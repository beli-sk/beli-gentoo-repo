My personal ebuild repository for Gentoo
========================================

Install
-------

Obtain following signing key (may contain also other UIDs):

```
pub   ed25519/0xF20826BE 2021-08-26 [SC] [expires: 2025-01-31]
      Key fingerprint = 0062 2CF3 3893 390C 1A7B  D94B 4A7A 129B F208 26BE
uid           Michal Belica <code@beli.sk>
sub   cv25519/0x1B275E50 2021-08-26 [E] [expires: 2025-01-31]
```

Either using WKD:

```
gpg --auto-key-locate wkd --locate-key code@beli.sk
```

or from keys.openpgp.org

```
gpg --fetch-key https://keys.openpgp.org/vks/v1/by-fingerprint/00622CF33893390C1A7BD94B4A7A129BF20826BE
```

Then make a minimal export with single UID for use by portage and place the
resulting file at a location referenced later in `repos.conf`.

```
gpg -o beli.gpg --export-options export-minimal \
  --export-filter keep-uid=uid=~code@beli.sk \
  --export 00622CF33893390C1A7BD94B4A7A129BF20826BE
```

Create `/etc/portage/repos.conf/beli.conf` with content

```
# /etc/portage/repos.conf/beli.conf
[beli]
auto-sync = yes
location = /var/db/repos/beli
sync-type = git
sync-uri = https://gitlab.com/beli-sk/beli-gentoo-repo.git
sync-git-verify-commit-signature = true
sync-openpgp-key-path = /etc/portage/keys/beli.gpg
sync-openpgp-keyserver = hkps://keys.openpgp.org 
sync-openpgp-key-refresh-retry-count = 40
sync-openpgp-key-refresh-retry-overall-timeout = 1200
sync-openpgp-key-refresh-retry-delay-exp-base = 2
sync-openpgp-key-refresh-retry-delay-max = 60
sync-openpgp-key-refresh-retry-delay-mult = 4
```

Sync & enjoy

```
emaint sync -r beli
```

