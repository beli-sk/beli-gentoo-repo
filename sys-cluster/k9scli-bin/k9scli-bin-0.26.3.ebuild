# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="terminal based UI to manage kubernetes clusters"
HOMEPAGE="https://k9scli.io"
SRC_URI="https://github.com/derailed/k9s/releases/download/v${PV}/k9s_Linux_x86_64.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"

QA_PREBUILT="
	${DESTDIR#/}usr/bin/k9s
"

S="${WORKDIR}"

src_install() {
	dobin k9s
	dodoc -r LICENSE README.md
}
