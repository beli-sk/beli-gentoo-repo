# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Customization of kubernetes YAML configurations"
HOMEPAGE="https://kustomize.io/"
SRC_URI="https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv${PV}/kustomize_v${PV}_linux_amd64.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"

QA_PREBUILT="
	${DESTDIR#/}usr/bin/kustomize
"

S="${WORKDIR}"

src_install() {
	dobin kustomize
}
